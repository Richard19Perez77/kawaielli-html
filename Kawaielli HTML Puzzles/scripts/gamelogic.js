var canvas;
var context;
var sectionWidth;
var sectionHeight;
var mouseX = 0;
var mouseY = 0;
var movingPiece = false;
var currentMovingFromPiece = -1;
var currentMovingToPiece = 0;
var offsetX = 0;
var offsetY = 0;
var imageObj;
var solved = false;
var currentImageNumber = 0;
var imageObj = new Image();
var PIECES = 9;
var SIDES = 3;
var isAnimating = false;

var IMAGECOUNT = 90;

var artTitle = new Array();
artTitle[0] = 'Alice In Wonderland.jpg';
artTitle[1] = 'Anemone.jpg';
artTitle[2] = 'Anglerfish 2.jpg';
artTitle[3] = 'Anglerfish.jpg';
artTitle[4] = 'Asuka (1).jpg';
artTitle[5] = 'Asuka (10).jpg';
artTitle[6] = 'Asuka (11).jpg';
artTitle[7] = 'Asuka (12).jpg';
artTitle[8] = 'Asuka (2).jpg';
artTitle[9] = 'Asuka (3).jpg';
artTitle[10] = 'Asuka (4).jpg';
artTitle[11] = 'Asuka (5).jpg';
artTitle[12] = 'Asuka (6).jpg';
artTitle[13] = 'Asuka (7).jpg';
artTitle[14] = 'Asuka (8).jpg';
artTitle[15] = 'Asuka (9).jpg';
artTitle[16] = 'Bizengast (1).jpg';
artTitle[17] = 'Bizengast (2).jpg';
artTitle[18] = 'Dark Misty.jpg';
artTitle[19] = 'Dragons Nest.jpg';
artTitle[20] = 'Elsa  (1).jpg';
artTitle[21] = 'Elsa  (2).jpg';
artTitle[22] = 'Elsa  (3).jpg';
artTitle[23] = 'Ero Lolita (1).jpg';
artTitle[24] = 'Ero Lolita (2).jpg';
artTitle[25] = 'Everlasting Summer (1).jpg';
artTitle[26] = 'Everlasting Summer (2).jpg';
artTitle[27] = 'Everlasting Summer (3).jpg';
artTitle[28] = 'Everlasting Summer (4).jpg';
artTitle[29] = 'Everlasting Summer.jpg';
artTitle[30] = 'Gumi Megpoid.jpg';
artTitle[31] = 'Gumi.jpg';
artTitle[32] = 'Harley Quinn 2.jpg';
artTitle[33] = 'Harley Quinn.jpg';
artTitle[34] = 'Haruhi Suzumiya.jpg';
artTitle[35] = 'Heather Mason.jpg';
artTitle[36] = 'Jinx.jpg';
artTitle[37] = 'Junko.jpg';
artTitle[38] = 'Karuto (1).jpg';
artTitle[39] = 'Karuto (2).jpg';
artTitle[40] = 'Karuto (3).jpg';
artTitle[41] = 'Kirino (1).jpg';
artTitle[42] = 'Kirino (2).jpg';
artTitle[43] = 'Kirino (3).jpg';
artTitle[44] = 'Kuroko.jpg';
artTitle[45] = 'Lolita.jpg';
artTitle[46] = 'Madoka Kaname.jpg';
artTitle[47] = 'Makoto Kino (1).jpg';
artTitle[48] = 'Makoto Kino (2).jpg';
artTitle[49] = 'Marceline.jpg';
artTitle[50] = 'Me in 2013.jpg';
artTitle[51] = 'Me in 2015.jpg';
artTitle[52] = 'Megumi Shimizu (1).jpg';
artTitle[53] = 'Megumi Shimizu.jpg';
artTitle[54] = 'Miku (1).jpg';
artTitle[55] = 'Miku (2).jpg';
artTitle[56] = 'Miku (3).jpg';
artTitle[57] = 'Miku (4).jpg';
artTitle[58] = 'Miku Hatsune (1).jpg';
artTitle[59] = 'Miku Hatsune (2).jpg';
artTitle[60] = 'Miku Hatsune (3).jpg';
artTitle[61] = 'Miku Hatsune (4).jpg';
artTitle[62] = 'Miku Hatsune (5).jpg';
artTitle[63] = 'Millia Rage (1).jpg';
artTitle[64] = 'Millia Rage (2).jpg';
artTitle[65] = 'Millia Rage (3).jpg';
artTitle[66] = 'Naruto.jpg';
artTitle[67] = 'Original Summer Elf.jpg';
artTitle[68] = 'Princess Frog.jpg';
artTitle[69] = 'Rei Miyamoto (1).jpg';
artTitle[70] = 'Rei Miyamoto (2).jpg';
artTitle[71] = 'Rider.jpg';
artTitle[72] = 'Rin Tohsaka (1).jpg';
artTitle[73] = 'Rin Tohsaka (2).jpg';
artTitle[74] = 'Rouge the bat (1).jpg';
artTitle[75] = 'Rouge the bat (2).jpg';
artTitle[76] = 'Saber Lily (1).jpg';
artTitle[77] = 'Saber Lily (2).jpg';
artTitle[78] = 'Sailor Fuki (1).jpg';
artTitle[79] = 'Sailor Fuki (2).jpg';
artTitle[80] = 'Sailor Moon.jpg';
artTitle[81] = 'Sonsaku Hakufu.jpg';
artTitle[82] = 'Star Butterfly.jpg';
artTitle[83] = 'Trinity Blood.jpg';
artTitle[84] = 'Yuno Gasai (1).jpg';
artTitle[85] = 'Yuno Gasai (2).jpg';
artTitle[86] = 'Yuno Gasai (3).jpg';
artTitle[87] = 'Yuno Gasai (4).jpg';
artTitle[88] = 'Yuno Gasai (5).jpg';
artTitle[89] = 'Zero No Tsukiama.jpg';

var artist = 'Kawaielli';

var deviantArtLink ='http://kawaielli.deviantart.com/';

function Piece() {
	var pieceNumber;
	var x;
	var y;
}

var pieceArray = new Array();

function Slot() {
	var slotNumber;
	var x;
	var y;
	var Piece;
}

var slotArray = new Array();

function init(document) {
	canvas = document.getElementById('puzzleCanvas');
	//currentImageNumber = Math.floor(Math.random() * IMAGECOUNT);
	currentImageNumber = 0;
	var canvasOffset = $("#puzzleCanvas").offset();
	offsetX = Math.round(canvasOffset.left);
	offsetY = Math.round(canvasOffset.top);
	sectionWidth = canvas.width / SIDES;
	sectionHeight = canvas.height / SIDES;
	canvas.addEventListener("mousedown", doMouseDown, false);
	canvas.addEventListener("mouseup", doMouseUp, false);
	canvas.addEventListener("mousemove", doMouseMove, false);
	canvas.addEventListener("mouseout", doMouseOut, false);
	context = canvas.getContext('2d');

	document.getElementById("savePuzzleButton").addEventListener("click",
			function() {
				// creating a literal object "progress" filled with values from
				// inputs
				var progress = {
					currentImageNumber : currentImageNumber,
					pieces : PIECES,
					sides : SIDES,
					slotArray : slotArray,
					pieceArray : pieceArray
				};

				localStorage["progress"] = JSON.stringify(progress);

			});

	document.getElementById("loadPuzzleButton").addEventListener("click",
			function() {
				// decoding "progress" from local storage
				$('#artistTitleDiv').finish();
				$('#buttonDiv').finish();
				var progress = JSON.parse(localStorage.getItem("progress"));
				currentImageNumber = progress.currentImageNumber;
				slotArray = progress.slotArray;
				pieceArray = progress.pieceArray;
				PIECES = progress.pieces;
				SIDES = progress.sides;
				sectionWidth = canvas.width / SIDES;
				sectionHeight = canvas.height / SIDES;
				solved = false;
				loadPreviousImage();
			});

	document.getElementById("nextImageButton").addEventListener("click",
			function() {
				$('#artistTitleDiv').finish();
				$('#buttonDiv').finish();
				currentImageNumber++;
				if (currentImageNumber >= IMAGECOUNT) {
					currentImageNumber = 0;
				}
				initArrays();
				loadImage();
			});

	document.getElementById("changeDifficultyButton").addEventListener("click",
			function() {
				$('#artistTitleDiv').finish();
				$('#buttonDiv').finish();
				if (SIDES > 5) {
					SIDES = 2;
					PIECES = SIDES * SIDES;
				} else {
					SIDES++;
					PIECES = SIDES * SIDES;
				}

				if (currentImageNumber >= IMAGECOUNT) {
					currentImageNumber = 0;
				}
				
				sectionWidth = canvas.width / SIDES;
				sectionHeight = canvas.height / SIDES;
				initArrays();
				loadImage();
			});
	initArrays();
	loadImage();
}

function clearCanvas() {
	context = canvas.getContext('2d');
	context.save();

	// Use the identity matrix while clearing the canvas
	context.setTransform(1, 0, 0, 1, 0, 0);
	context.clearRect(0, 0, canvas.width, canvas.height);

	// Restore the transform
	context.restore();
	context.fillStyle = "#138AEF";
	context.fillRect(0, 0, canvas.width, canvas.height);
}

function initArrays() {
	pieceArray = new Array();
	slotArray = new Array();
	for (i = 0; i < PIECES; i++) {
		pieceArray[i] = new Piece();
		pieceArray[i].pieceNumber = i;
		slotArray[i] = new Slot();
		slotArray[i].slotNumber = i;
		slotArray[i].piece = pieceArray[i];
	}

	for (y = 0; y < SIDES; y++) {
		for (x = 0; x < SIDES; x++) {
			var index = x + (y * SIDES);
			var w = x * sectionWidth;
			var h = y * sectionHeight;
			slotArray[index].x = w;
			slotArray[index].y = h;
			pieceArray[index].x = w;
			pieceArray[index].y = h;
		}
	}
}

function adjustOffset() {
	var canvasOffset = $("#puzzleCanvas").offset();
	offsetX = Math.round(canvasOffset.left);
	offsetY = Math.round(canvasOffset.top);
}

function loadPreviousImage() {
	isAnimating = true;
	var t = "" + artTitle[currentImageNumber];
	var u = t.indexOf("_");
	var d = t.indexOf(".");
	var titleName = t.substring(u + 1, d);
	while (titleName.indexOf("_") != -1) {
		titleName = titleName.replace("_", " ");
	}
	var artistNameAndDeviantArtLink = "<a href="
			+ deviantArtLink + " target=\"_blank\">"
			+ artist + "</a>: " + titleName;
	$('#artistTitleDiv').slideUp();
	$('#buttonDiv').slideUp();
	imageObj.src = 'images/' + artTitle[currentImageNumber];
	imageObj.onload = function() {
		$('#artistTitleDiv').slideDown(400, function() {
			$('#artistTitleText').html(artistNameAndDeviantArtLink);
			isAnimating = false;
			checkForAllPiecesInCorrectSlot();
			drawCanvas();
		});
		$('#buttonDiv').slideDown();
	}
}

function loadImage() {
	isAnimating = true;
	solved = false;
	var t = "" + artTitle[currentImageNumber];
	var u = t.indexOf("_");
	var d = t.indexOf(".");
	var titleName = t.substring(u + 1, d);
	while (titleName.indexOf("_") != -1) {
		titleName = titleName.replace("_", " ");
	}
	var artistNameAndDeviantArtLink = "<a href="
			+ deviantArtLink + " target=\"_blank\">"
			+ artist + "</a>: " + titleName;
	$('#artistTitleDiv').slideUp();
	$('#buttonDiv').slideUp();
	imageObj.src = 'images/' + artTitle[currentImageNumber];
	imageObj.onload = function() {
		putPiecesInRandomSlots();
		$('#artistTitleDiv').slideDown(400, function() {
			$('#artistTitleText').html(artistNameAndDeviantArtLink);
			isAnimating = false;
			checkForAllPiecesInCorrectSlot();
			drawCanvas();
		});
		$('#buttonDiv').slideDown();
	}
}

function doMouseDown(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		currentMovingFromPiece = getCurrentPiece(mouseX, mouseY);
		drawCanvas();
	}
}

function doMouseMove(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		drawCanvas();
	}
}

function doMouseUp(event) {
	if (solved == false && isAnimating == false) {
		mouseX = event.clientX - offsetX;
		mouseY = event.clientY - offsetY;
		currentMovingToPiece = getCurrentPiece(mouseX, mouseY);
		switchPieces(currentMovingFromPiece, currentMovingToPiece);
		currentMovingFromPiece = -1;
		checkForAllPiecesInCorrectSlot();
		drawCanvas();
	}
}

function checkForAllPiecesInCorrectSlot() {
	var piecesSolved = 0;
	for (var int = 0; int < PIECES; int++) {
		if (slotArray[int].slotNumber == slotArray[int].piece.pieceNumber) {
			piecesSolved++;
		}
	}
	if (piecesSolved == PIECES) {
		solved = true;
	}
}

function doMouseOut(event) {
	currentMovingFromPiece = -1;
	drawCanvas();
}

function getCurrentPiece(x, y) {
	var xIndex = 0;
	var tempSectionWidth = sectionWidth;
	while (x >= tempSectionWidth) {
		xIndex++;
		tempSectionWidth += sectionWidth;
	}

	var yIndex = 0;
	var tempSectionHeight = sectionHeight;
	while (y >= tempSectionHeight) {
		yIndex++;
		tempSectionHeight += sectionHeight;
	}

	var pieceIndex = xIndex + (yIndex * SIDES);

	return pieceIndex;
}

function putPiecesInRandomSlots() {
	var isRandom = false;
	var attempts = 0;
	var moves = 0;
	if (PIECES % 2 == 0) {
		moves = PIECES / 2;
	} else {
		moves = (PIECES / 2) + 1;
	}
	while (!isRandom) {
		attempts++;
		for (var from = 0; from < moves; from++) {
			var to = Math.floor(Math.random() * PIECES);
			while (from == to) {
				to = Math.floor(Math.random() * PIECES);
			}
			switchPieces(from, to);
		}

		isRandom = true;
		for (var int = 0; int < slotArray.length; int++) {
			if (slotArray[int].slotNumber == slotArray[int].piece.pieceNumber) {
				isRandom = false;
			}
		}
	}
}

function switchPieces(fromSlot, toSlot) {
	var tempPiece = slotArray[toSlot].piece;
	slotArray[toSlot].piece = slotArray[fromSlot].piece;
	slotArray[fromSlot].piece = tempPiece;
}

function drawCanvas() {
	// Store the current transformation matrix
	clearCanvas();
	drawPieces();
}

function drawBorder() {
	context.strokeStyle = "#000000";
	context.lineWidth = 3;
	context.beginPath();

	var tempWidth;
	var tempHeight;
	for (var int = 1; int <= SIDES - 1; int++) {
		tempWidth = sectionWidth * int;

		context.moveTo(tempWidth, 0);
		context.lineTo(tempWidth, 0);
		context.lineTo(tempWidth, canvas.height);

		tempHeight = sectionHeight * int;

		context.moveTo(0, tempHeight);
		context.lineTo(0, tempHeight);
		context.lineTo(canvas.width, tempHeight);
	}

	context.stroke();
}

function drawPieces() {
	for (var int = 0; int < PIECES; int++) {
		var slot = slotArray[int];
		var piece = slotArray[int].piece;
		var movingPiece;
		if (currentMovingFromPiece != int) {
			context.drawImage(imageObj, piece.x, piece.y, sectionWidth,
					sectionHeight, slot.x, slot.y, sectionWidth, sectionHeight);
		} else {
			movingPiece = piece;
			context.globalAlpha = 0.8;
			context.drawImage(imageObj, piece.x, piece.y, sectionWidth,
					sectionHeight, slot.x, slot.y, sectionWidth, sectionHeight);
			context.fillStyle = "#fefa5c";
			context.fillRect(slot.x, slot.y, sectionWidth, sectionHeight);
			context.globalAlpha = 1;
		}

		if (solved == false) {
			drawBorder();
		}

		if (movingPiece != null) {
			var centerX = mouseX - sectionWidth / 2;
			var centerY = mouseY - sectionHeight / 2;
			context.drawImage(imageObj, movingPiece.x, movingPiece.y,
					sectionWidth, sectionHeight, centerX, centerY,
					sectionWidth, sectionHeight);
		}
	}
}